<?php

/*
 * Implements hook_form_alter().
 */
function autouser_form_alter(&$form, $form_state, $form_id) {
  global $user;
  if ($form_id == 'node_type_form' && isset($form['#node_type']->type)) {
    $node_type = $form['#node_type']->type;
    $node_type_settings = autouser_get_settings($node_type);
      $email_fields = _get_node_fields($node_type, 'email');
      $text_fields = array(
        'use_email' => t('[part of email: {username}@xxxx.yy]'), 
        'title' => t('Node title'),
    );
    $text_fields += _get_node_fields($node_type, 'text');
        
    $form['#validate'][] = 'autouser_content_type_validate';
    
    if (!isset($form['autouser'])) {
      $form['autouser'] = array();
    }
    $form['autouser'] += array(
      '#type' => 'fieldset',
      '#title' => t('Auto User module settings'),
      '#collapsible' => TRUE,
      '#collapsed' => isset($node_type_settings['enabled']) ? !($node_type_settings['enabled']) : TRUE,
      '#group' => 'additional_settings',
          '#tree' => TRUE,
      '#weight' => 0
    );
    $form['autouser']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto User'),
      '#description' => empty($email_fields) ? t('Add at least one <a href="!url">email field</a> to this content type to enable Auto User feature!', array('!url' => url('admin/structure/types/manage/' . $node_type . '/fields'))) : t('Check if you want to use Auto User behavior.'),
      '#disabled' => empty($email_fields) ? TRUE : FALSE,
      '#default_value' => isset($node_type_settings['enabled']) && !empty($email_fields) ? $node_type_settings['enabled'] : FALSE,
      '#weight' => 0,
    );
    if (!empty($email_fields)) {
      $form['autouser']['email_field'] = array(
        '#type' => 'select',
        '#title' => t('Email field'),
        '#description' => t('Select the email field to provide email address for newly created user.'),
        '#options' => $email_fields,
        '#default_value' => isset($node_type_settings['email_field']) ? $node_type_settings['email_field'] : 0,
      );
      $form['autouser']['name_field'] = array(
        '#type' => 'select',
        '#title' => t('Name field'),
        '#description' => t('Select the user name field.'),
        '#options' => $text_fields,
        '#default_value' => isset($node_type_settings['name_field']) ? $node_type_settings['name_field'] : 0,
      );
      $roles = user_roles();
      $auth_role = $roles[2];
      unset($roles[1]);
      unset($roles[2]);

      $form['autouser']['autouser_role'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Roles to assign to auto-created user.'),
        '#default_value' => isset($node_type_settings['autouser_role']) ? $node_type_settings['autouser_role'] : array(),
        '#options' => $roles,
        array(
          '#type' => 'checkbox',
          '#title' => $auth_role,
          '#default_value' => 2,
          '#disabled' => TRUE,
          '#return_value' => 2,
        ),
      );
      $form['autouser']['autouser_user_status'] = array(
        '#type' => 'radios',
        '#title' => t('Default user status.'),
        '#default_value' => isset($node_type_settings['autouser_user_status']) ? $node_type_settings['autouser_user_status'] : 0,
        '#options' => array(0 => t('Blocked'), 1 => t('Active')),
      );
      $form['autouser']['author'] = array(
        '#type' => 'checkbox',
        '#title' => t('Assign user as author of the node created.'),
        '#description' => t('If not checked - content will be authored by Anonymous.'),
        '#default_value' => isset($node_type_settings['author']) ? $node_type_settings['author'] : TRUE,
      );
      $form['autouser']['publish'] = array(
        '#type' => 'checkbox',
        '#title' => t('Publish content'),
        '#description' => t('Check if you want new node to be published immediately.'),
        '#default_value' => isset($node_type_settings['publish']) && !empty($email_fields) ? $node_type_settings['publish'] : FALSE,
      );
    }
    else {
      $node_type_settings['enabled'] = 0;
      _autouser_store_settings(array('node_type' => $node_type), $node_type_settings);
    }

    $form['#submit'][] = 'autouser_content_type_submit';
  }
  
  if ($user->uid == 0 && isset($form['#node_edit_form'])) {
    if (autouser_get_settings($form['#node']->type, 1)) {
      $form['#validate'][] = 'autouser_node_validate';
      array_unshift($form['actions']['submit']['#submit'], 'autouser_node_submit');
    }
  }
}

function _get_node_fields($node_type, $field_type) {
  $node_fields = field_info_instances('node', $node_type);
  $fields = array();
  foreach ($node_fields as $field) {
    $field_info = field_info_field_by_id($field['field_id']);
    if ($field_info['type'] == $field_type) {
      $fields[$field['field_name']] = $field['field_name'];
    }
  }
  
  return $fields;
}

function autouser_node_validate($form, &$form_state) {
  if (isset($form_state['values']['type']) && is_string($form_state['values']['type'])) {
    $node_language = $form_state['values']['language'] ? $form_state['values']['language'] : 'und';
    $au_settings = autouser_get_settings($form_state['values']['type']);
    $au_email_field = $au_settings['email_field'];
    $au_name_field = ($au_settings['name_field'] != 'use_email') ? $au_settings['name_field'] : FALSE;
    switch ($au_name_field) {
      case FALSE:
        break;
      case 'title': 
        if ($form_state['values']['title'] != '') {
          $au_username = $form_state['values']['title'];
          $au_validate_field_title = $form['title']['#title'];
        }
        break;
      default:
        if ($form_state['values'][$au_name_field][$node_language][0]['value'] != '') {
          $au_username = $form_state['values'][$au_name_field][$node_language][0]['value'];
          $au_validate_field_title = $form[$au_name_field][$node_language]['#title'];
        }
    }
    if (isset($au_username) && isset($au_validate_field_title) && $au_name_field) {
      if ($message = autouser_validate_name($au_username, $au_validate_field_title)) {
        form_set_error($au_name_field, $message);
      }
    }
    
    // Check if user already exists
    if (isset($form_state['values'][$au_email_field][$node_language][0]['email'])) {
      
      $au_user_email = $form_state['values'][$au_email_field][$node_language][0]['email'];
      
      $account = _au_user_exists($au_user_email);
      if ($account) {
        $message = t('User with email %email already exists. Please <a href="!login_url">login</a> if you want to continue with this email address or enter another one.', array('%email' => $au_user_email, '!login_url' => url('user')));
        form_set_error($au_email_field, $message);
      }
      else {
      //  drupal_set_message('New user detected!');
      }
    }
  }
}

function autouser_content_type_submit($form, &$form_state) {
  $type = !empty($form['#node_type']->type) ? $form['#node_type']->type : $form['#post']['type'];
  _autouser_store_settings(array('node_type' => $type), $form_state['values']['autouser']);
}

function autouser_get_settings($node_type, $status = 0) {
  if (isset($node_type) && is_string($node_type)) {
    if ($settings = variable_get('autouser_settings_' . $node_type, FALSE)) {
      if ($status) {
      
        return $settings['enabled'];
      }
      else {
      
        return $settings;
      }
    }
  }
  
  return FALSE;
}

function _autouser_store_settings($args, $settings) {
  if (isset($args['node_type'])) {
    variable_set('autouser_settings_' . $args['node_type'], $settings);
  }
}

function autouser_node_submit($form, &$form_state) {
	dpm($form_state);
  if (isset($form_state['values']['type']) && is_string($form_state['values']['type'])) {

		$node_language = $form_state['values']['language'] ? $form_state['values']['language'] : 'und';
		$au_settings = autouser_get_settings($form_state['values']['type']);
		$au_name_field = $au_settings['name_field'];
		$au_email_field = $au_settings['email_field'];

    // set publish/unpublish state
    if ($au_settings['publish']) {
      $form_state['values']['status'] = 1;
    }
    else {
      $form_state['values']['status'] = 0;
    }

    if (isset($form_state['values'][$au_email_field][$node_language][0])) {
      reset($form_state['values'][$au_email_field][$node_language][0]);
      list($k, $email_value) = each($form_state['values'][$au_email_field][$node_language][0]);
      if ($email_value != '') {
        $roles = user_roles();
        if ($uid = db_query("SELECT uid FROM {users} WHERE mail = :mail;", array(':mail' => $email_value))->fetchField()) {
          if ($au_settings['author']) {
            $_SESSION['autouser']['uid'] = $uid;
          }

          // TODO Convert "user_load" to "user_load_multiple" if "$uid" is other than a uid.
          // To return a single user object, wrap "user_load_multiple" with "array_shift" or equivalent.
          // Example: array_shift(user_load_multiple(array(), $uid))
          $user = user_load($uid);
          $user_roles = $user->roles;
          foreach ($au_settings['autouser_role'] as $value) {
            if ($value && !array_key_exists($value, $user_roles)) {
              $user_roles[$value] = $roles[$value];
            }
          }
          user_save($user, array('roles' => $user_roles));
          return;
        }

        if ($au_name_field == 'title' && $form_state['values']['title'] != '') {
          $name = $form_state['values']['title'];
        }
        elseif ($au_name_field && isset($form_state['values'][$au_name_field][$node_language][0]['value']) && $form_state['values'][$au_name_field][$node_language][0]['value'] != '') {
          $name = $form_state['values'][$au_name_field][$node_language][0]['value'];
        }
        else {
          $name = explode("@", $email_value);
          $name = $name[0];
        }

        $roles_active = array(2 => $roles[2]);
        foreach ($au_settings['autouser_role'] as $value) {
          if ($value) {
            $roles_active[$value] = $roles[$value];
          }
        }
        ksort($roles_active);

        //(bool) db_query_range('SELECT 1 FROM {mytable}', 0, 1)->fetchField()
        if ((bool) db_query_range("SELECT 1 FROM {users} WHERE name = :name;", 0, 1, array(':name' => $name))->fetchField()) {
          $name_postfix = 0;
          $name_check = $name . $name_postfix;
          while ((bool) db_query_range("SELECT 1 FROM {users} WHERE name = :name;", 0, 1, array(':name' => $name_check))->fetchField()) {
            $name_postfix++;
            $name_check = $name . $name_postfix;
          }
          $name .= $name_postfix;
        }
        $u = array(
          'name' =>  $name,
          'mail' => $email_value,
          'status' => 0,
          'init' => $email_value,
          'autouser' => TRUE,
          'roles' => $roles_active,
        );
        global $_au_user;
        $_au_user = 1;
        $u = user_save('', $u);
        if ($u && $au_settings['author']) {
          $_SESSION['autouser']['uid'] = $u->uid;
        }
        unset($_au_user);

        // Activate user account
        if ($au_settings['autouser_user_status']) {
          user_save($u, array('status' => 1));
        }
      }
    }
  }
}

function autouser_node_presave($node) {
  if (isset($_SESSION['autouser']['uid'])) {
    $node->uid = $_SESSION['autouser']['uid'];
    unset($_SESSION['autouser']['uid']);
  }
}


//Get user by email
// Returns user object if user with $email exists or FALSE if it's not
function _au_user_exists($email) {
  $account = user_load_by_mail($email);      
  if (isset($account)) {
    return $account;  
  }
  return FALSE;
}

/**
 * Verify the syntax of the given user name.
 */
function autouser_validate_name($name, $field) {
  if (substr($name, 0, 1) == ' ') {  
    return t('The field %field cannot begin with a space.', array('%field' => $field));
  }
  if (substr($name, -1) == ' ') {
  
    return t('The value of field %field cannot end with a space.', array('%field' => $field));
  }
  if (strpos($name, '  ') !== FALSE) {
  
    return t('The field %field cannot contain multiple spaces in a row.', array('%field' => $field));
  }
  if (preg_match('/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i', $name)) {
  
    return t('The field %field contains an illegal character.', array('%field' => $field));
  }
  if (preg_match('/[\x{80}-\x{A0}' .          // Non-printable ISO-8859-1 + NBSP
                   '\x{AD}' .                 // Soft-hyphen
                   '\x{2000}-\x{200F}' .      // Various space characters
                   '\x{2028}-\x{202F}' .      // Bidirectional text overrides
                   '\x{205F}-\x{206F}' .      // Various text hinting characters
                   '\x{FEFF}' .               // Byte order mark
                   '\x{FF01}-\x{FF60}' .      // Full-width latin
                   '\x{FFF9}-\x{FFFD}' .      // Replacement characters
                   '\x{0}-\x{1F}]/u',         // NULL byte and control characters
                   $name)) {
    return t('The username contains an illegal character.', array('%field' => $field));
  }
  if (drupal_strlen($name) > USERNAME_MAX_LENGTH) {
  
    return t('The value of field %field is too long: it must be %max characters or less.', array('%field' => $field, '%max' => USERNAME_MAX_LENGTH));
  }
  
  return FALSE;
}

