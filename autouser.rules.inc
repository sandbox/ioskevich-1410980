<?php
// $Id: autouser.rules.inc,v 1.0 2010/12/02 Sergei Churilo Exp $

/**
 * Implementation of hook_rules_condition_info().
 */
function autouser_rules_condition_info() {
  return array(
    'autouser_condition_user_is_autouser' => array(
      'label' => t('User is auto created'),
      'arguments' => array(
        'node' => array(
          'type' => 'user',
          'label' => t('User'),
        ),
      ),
      'module' => 'Autouser',
    ),
  );
}

/**
 * Check to see if the user is autouser.
 */
function autouser_condition_user_is_autouser($user) {
  global $autouser;      
  if($autouser) {
    return TRUE;
  }
  return FALSE; 
}
